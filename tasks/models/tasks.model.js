import Mongoose from "mongoose";
import connectToDb from "../../db";
import "babel-polyfill";

connectToDb();
//Define a schema
const Schema = Mongoose.Schema;

const TaskSchema = new Schema(
  {
    title: { type: String, required: true, trim: true },
    description: { type: String, required: true, trim: true },
    dueDate: { type: Date, required: true }
  },
  {
    collection: "Tasks"
  }
);
let TaskModel = Mongoose.model("Tasks", TaskSchema);
TaskModel.getCount = async () => {
  return await new Promise((resolve, reject) => {
    return TaskModel.countDocuments().exec(function(err, total) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        resolve(total);
      }
    });
  });
};
TaskModel.getAll = async (perPage = 10, page = 0) => {
  return await new Promise((resolve, reject) => {
    TaskModel.find()
      .sort({ _id: -1 })
      .limit(perPage)
      .skip(perPage * page)
      .exec(function(err, tasks) {
        if (err) {
          reject(err);
        } else {
          resolve(tasks);
        }
      });
  });
};

TaskModel.addTask = task => {
  return task.save();
};
TaskModel.updateTask = async (id, task) => {
  return await new Promise((resolve, reject) => {
    TaskModel.updateOne({ _id: id }, { $set: task }, { upsert: true }).exec(
      function(err, task) {
        if (err) {
          console.log(err);
          return reject(err);
        } else {
          task;
          console.log(task);
          return resolve(task);
        }
      }
    );
  });
};
TaskModel.removeTask = async id => {
  return await new Promise((resolve, reject) => {
    TaskModel.findByIdAndDelete(id).exec(function(err, task) {
      if (err) {
        reject(err);
      } else {
        resolve(task);
      }
    });
  });
};
TaskModel.getById = async id => {
  return await new Promise((resolve, reject) => {
    TaskModel.findById(id).exec(function(err, task) {
      if (err) {
        reject(err);
      } else {
        resolve(task);
      }
    });
  });
};

export default TaskModel;
