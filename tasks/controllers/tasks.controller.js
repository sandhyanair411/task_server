import TaskModel from "../models/tasks.model";
const TaskController = {};

TaskController.getAll = (req, res) => {
  try {
    // max response items in one request is 100
    let limit =
      req.query.limit && req.query.limit <= 100
        ? parseInt(req.query.limit)
        : 10;
    let page = 0;
    if (req.query) {
      if (req.query.page) {
        req.query.page = parseInt(req.query.page);
        page = Number.isInteger(req.query.page) ? req.query.page : 0;
      }
    }
    let status = false;
    let data = null;
    let totalRecords = 0;
    Promise.all([TaskModel.getAll(limit, page), TaskModel.getCount()])
      .then(function(response) {
        data = response[0];
        totalRecords = response[1];
      })
      .finally(() => {
        res.status(200).json({
          status: true,
          totalRecords,
          limit,
          totalPages: Math.ceil(totalRecords / limit),
          currentPage: page + 1,
          data
        });
      });
  } catch (err) {
    res.json(err);
  }
};

TaskController.addTask = (req, res) => {
  const task = TaskModel({
    title: req.body.title,
    description: req.body.description,
    dueDate: req.body.dueDate
  });
  try {
    let message = "";
    let responseCode = 200;
    let data = null;
    let totalRecords = 0;
    Promise.all([TaskModel.addTask(task), TaskModel.getCount()])
      .then(response => {
        data = response[0]._doc;
        totalRecords = response[1];
        message = "Task Added successfully!";
      })
      .catch(error => {
        responseCode = 400;
        data = error;
        message = "Exception Occurred!!";
      })
      .finally(() => {
        res.status(responseCode).json({
          totalRecords,
          data,
          message
        });
      });
  } catch (err) {
    res.send("Got error in addTask");
  }
};
TaskController.updateTask = (req, res) => {
  const task = TaskModel({
    title: req.body.title,
    description: req.body.description,
    dueDate: req.body.dueDate
  });
  try {
    let message = "";
    let responseCode = 200;
    let data = null;
    TaskModel.updateTask(request.params.taskId, task)
      .then(response => {
        data = response;
        message = "Task Updated Successfully!";
      })
      .catch(error => {
        responseCode = 400;
        data = error;
        message = "Exception Occurred!!";
      })
      .finally(() => {
        res.status(responseCode).json({
          data,
          message
        });
      });
  } catch (err) {
    res.status(400).json({
      data: err
    });
  }
};
TaskController.deleteTask = (req, res) => {
  let { taskId } = req.params;
  try {
    let message = "";
    let responseCode = 200;
    let data = null;
    let totalRecords = 0;
    Promise.all([TaskModel.removeTask(taskId), TaskModel.getCount()])
      .then(response => {
        // console.log(response);
        let result = response[0];
        responseCode = 200;
        message = "Task Deleted successfully!";
        totalRecords = response[1];
      })
      .catch(error => {
        // console.log(error);
        responseCode = 400;
        data = error;
        message = "Exception Occurred!!";
      })
      .finally(() => {
        res.status(responseCode).json({
          totalRecords,
          _id: taskId,
          data,
          message
        });
      });
  } catch (err) {
    res.send("Got error in delete task!");
  }
};
TaskController.getById = (req, res) => {
  let { taskId } = req.params;
  try {
    let responseCode = 200;
    let data = null;
    let message = "";
    TaskModel.getById(taskId)
      .then(response => {
        data = response;
      })
      .catch(error => {
        responseCode = 400;
      })
      .finally(() => {
        res.status(responseCode).json({
          _id: taskId,
          data,
          message
        });
      });
  } catch (err) {
    res.send("Got error in fetch task by id!");
  }
};
export default TaskController;
