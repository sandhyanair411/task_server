import express from "express";
import TaskController from "../tasks/controllers/tasks.controller";
const TasksRouter = express.Router();
TasksRouter.get("/", (req, res) => {
  TaskController.getAll(req, res);
});
TasksRouter.get("/:taskId", (req, res) => {
  TaskController.getById(req, res);
});
TasksRouter.post("/", (req, res) => {
  TaskController.addTask(req, res);
});

TasksRouter.delete("/:taskId", (req, res) => {
  TaskController.deleteTask(req, res);
});

TasksRouter.put("/:taskId", (req, res) => {
  TaskController.updateTask(req, res);
});
export default TasksRouter;
