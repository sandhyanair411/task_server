import Mongoose from "mongoose";
import { dbConfig } from "../config";
const connectToDb = () => {
  let dbHost = dbConfig.dbHost;
  let dbPort = dbConfig.dbPort;
  let dbName = dbConfig.dbName;
  try {
    Mongoose.connect(`mongodb://${dbHost}:${dbPort}/${dbName}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log(
      "Connected to mongo!!!" + `mongodb://${dbHost}:${dbPort}/${dbName}`
    );
  } catch (err) {
    console.log("Could not connect to MongoDB");
  }
};

export default connectToDb;
