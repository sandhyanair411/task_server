import express from "express";
import bodyParser from "body-parser";
import { appConfig } from "./config";
import TaskRouter from "./routes/tasks.routes";

const app = express();

const port = appConfig.port;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  // res.header('Access-Control-Allow-Credentials', 'true');
  res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE");
  // res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header(
    "Access-Control-Allow-Headers",
    "Accept, Authorization, Content-Type, X-Requested-With, Range"
  );
  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  } else {
    return next();
  }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/tasks", TaskRouter);
//Index route
app.get("/", function(req, res) {
  res.send("Invalid Request!!");
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
//
//

// const app = express();
// Mongoose.Promise = global.Promise;
// connectToDb();
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Credentials", "true");
//   res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE");
//   res.header("Access-Control-Expose-Headers", "Content-Length");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Accept, Authorization, Content-Type, X-Requested-With, Range"
//   );
//   if (req.method === "OPTIONS") {
//     return res.send(200);
//   } else {
//     return next();
//   }
// });

// app.use(bodyParser.json());

// TasksRouter.routesConfig(app);

// app.listen(config.port, function() {
//   console.log("app listening at port %s", config.port);
// });
